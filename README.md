# README #

Подробное описание доступно в статье: [Поиск графиков по шаблону через корреляцию](http://quantrum.me/625-poisk-grafikov-po-shablonu-cherez-korrelyaciyu/).

## Database fields (PostgreSQL) ##

* **symbol**	character varying(6)
* **dt**	date
* **open**	bigint [0]
* **high**	bigint [0]
* **low**	bigint [0]
* **close**	bigint [0]
* **volume**	numeric(20,0) [0]
* **adj**	numeric(20,0) NULL

## Additional ##

Included example PHP script to download database from Quandl.